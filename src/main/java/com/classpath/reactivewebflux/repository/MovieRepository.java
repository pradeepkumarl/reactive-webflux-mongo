package com.classpath.reactivewebflux.repository;

import com.classpath.reactivewebflux.model.Movie;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import java.time.LocalDate;

@Repository
public interface MovieRepository extends ReactiveMongoRepository<Movie, String> {

    Flux<Movie> findByName(String name);

    Flux<Movie> findByRating(int rating);

    Flux<Movie> findByReleaseDateBetween(LocalDate startDate, LocalDate endDate);
}