package com.classpath.reactivewebflux.config;

import com.classpath.reactivewebflux.model.Movie;
import com.classpath.reactivewebflux.repository.MovieRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.time.LocalDate;
import java.util.UUID;

@Component
@Slf4j
@RequiredArgsConstructor
public class BootstrapInitializer implements ApplicationListener<ApplicationReadyEvent> {

    private final MovieRepository movieRepository;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        log.info("Application started event:: Bootstrapping initial data in MongoDB:: ");
        this.movieRepository
                .deleteAll()
                .thenMany(
                         Flux.just("movie-a", "movie-b", "movie-c")
                        .map(movieName -> Movie.builder().id(UUID.randomUUID().toString()).name(movieName).rating(4).releaseDate(LocalDate.now()).build())
                        .flatMap(this.movieRepository::save)
                )
                .thenMany(this.movieRepository.findAll())
                .subscribe(movie ->  log.info(" Movie save into the db successfully {}", movie.toString()));
    }
}