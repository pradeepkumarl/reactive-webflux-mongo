package com.classpath.reactivewebflux.service;

import com.classpath.reactivewebflux.model.Movie;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MovieService {

    Flux<Movie> fetchAll();

    Mono<Movie> findMovieById(String id);

    Mono<Movie> updateMovie(String id, Movie movie);

    Mono<Movie> save(Movie movie);

    Mono<Movie> deleteMovie(String id);
}