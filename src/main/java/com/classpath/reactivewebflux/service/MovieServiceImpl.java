package com.classpath.reactivewebflux.service;

import com.classpath.reactivewebflux.model.Movie;
import com.classpath.reactivewebflux.repository.MovieRepository;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@Service
@RequiredArgsConstructor
@Slf4j
public class MovieServiceImpl implements MovieService {

    private final MovieRepository movieRepository;

    @Override
    public Flux<Movie> fetchAll() {
        return this.movieRepository.findAll();
    }

    @Override
    public Mono<Movie> findMovieById(String id) {
        return this.movieRepository.findById(id);
    }

    @Override
    public Mono<Movie> save(Movie movie) {
        return this.movieRepository.save(movie);
    }

    @Override
    public Mono<Movie> updateMovie(String id, Movie movie) {
        Mono<Movie> updatedMovie = this.movieRepository
                .findById(id)
                .map(existingMovie -> Movie
                                         .builder()
                                         .id(existingMovie.getId())
                                         .name(movie.getName())
                                         .rating(movie.getRating())
                                         .releaseDate(movie.getReleaseDate())
                                         .build())
                .flatMap(this.movieRepository::save);
        return updatedMovie;
    }


    @Override
    public Mono<Movie> deleteMovie(String id) {
        //Mono<Void> updatedMovie = this.movieRepository.deleteById(id);
        Mono<Movie> deletedMovie = this.movieRepository
                .findById(id)
                .flatMap(movie -> this.movieRepository.delete(movie).thenReturn(movie));
        return deletedMovie;
    }
}