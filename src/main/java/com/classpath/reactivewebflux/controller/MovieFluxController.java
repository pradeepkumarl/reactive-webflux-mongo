package com.classpath.reactivewebflux.controller;

import com.classpath.reactivewebflux.model.Movie;
import com.classpath.reactivewebflux.service.MovieService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@RestController
@RequestMapping("/api/v1/movies")
@RequiredArgsConstructor
@Slf4j
public class MovieFluxController {

    private final MovieService movieService;

    @GetMapping
    public Flux<Movie> fetchAll(){
        log.info("Fetching movies from db::");
        return this.movieService
                .fetchAll();

    }

    @GetMapping("/{id}")
    public Mono<Movie> fetchAll(@PathVariable String id){
        log.info("Fetching movies from db:: {} ", id);
        return this.movieService.findMovieById(id);
    }

    @PostMapping
    public Mono<Movie> saveMovie(@RequestBody Movie movie){
        log.info("Saving the movie :: {}", movie.toString());
        return this.movieService.save(movie);
    }
}